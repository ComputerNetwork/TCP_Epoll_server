#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <netdb.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>

#include "common.h"

const char* HOST = "localhost"; //server host

const int NUMFILES = 7; //Number of files to be used in compression test
const int FNAMELEN = 7; //Max filename length
const int NCONNCURRENCY = 1;
const int TMAXDATALEN = 9000; //for testing
const int BUFFLEN = 1024;

//Console mutex (To avoid corrupted printf)
pthread_mutex_t console_mutex = PTHREAD_MUTEX_INITIALIZER;

/*
 * Author	: Sriramprabhu Sankaraguru
 * Email	: sriramprabhu.s@gmail.com
 * 
 * Compression Service Test suite:
 * -------------------------------
 * We have methods that helps in
 *  - testing individual requests (GET/PING/RST/COMPRESS)
 *  - executing test plan for each of these requests.
 *  - executing test plan for combined requests
 *  - executing test plan with concurrent clients
 *  - Compose and print header/payload based on input
 *
 * To test with concurrent clients, adjust NCONCURRENCY value
 *
 * Independent methods helps in devising test plans by picking just the 
 * tests we need.
 *
 */
/*
 * Utility to read data from fd completely (Extension/wrapper of read())
 */
static int read_all(int fd, char *buffer, int len) {

	int read_len = 0;
	
	while(len > 0) {
		int numbytes = read(fd, buffer + read_len, len);
		if(numbytes < 0) { //failure
			perror("Read ALL");
			break;
		}
		else if(numbytes == 0) {
			break;
		}		
		read_len += numbytes;
		len -= numbytes;	
	}	
	return read_len;
}

/*
 * Parse and print payload
 * Args:
 *  buffer - the payload as string of bytes
 * Returns:
 *  None
 */
static void print_stats(char *buffer) {

	uint32_t received_bytes, sent_bytes;
	uint8_t ratio;
	int offset = 0;
	
	memcpy(&received_bytes, buffer + offset, sizeof(uint32_t));	
	offset += sizeof(uint32_t);
	memcpy(&sent_bytes, buffer + offset, sizeof(uint32_t));
	offset += sizeof(uint32_t);
	memcpy(&ratio, buffer + offset, sizeof(uint8_t));
	
	//Convert to host byte order
	received_bytes = ntohl(received_bytes);
	sent_bytes = ntohl(sent_bytes);
	
	pthread_mutex_lock(&console_mutex);
	printf("Server Response Stats: Received: %d Sent: %d Ratio: %d\n", \
			received_bytes, sent_bytes, ratio);
	pthread_mutex_unlock(&console_mutex);

}

/*
 * Parse and print header
 * Args:
 *  buffer - the header as string of bytes
 * Returns:
 *  Payload length if SC is OK. FAILURE Otherwise
 */
static int parse_header(char *buffer) {

	uint32_t magic_number;
	uint16_t payload_length, status_code;
	int offset = 0;
	
	memcpy(&magic_number, buffer + offset, sizeof(uint32_t));	
	offset += sizeof(uint32_t);
	memcpy(&payload_length, buffer + offset, sizeof(uint16_t));
	offset += sizeof(uint16_t);
	memcpy(&status_code, buffer + offset, sizeof(uint16_t));
	
	//Convert to host byte order
	payload_length = ntohs(payload_length);
	status_code = ntohs(status_code);	
	
	pthread_mutex_lock(&console_mutex);
	printf("Server Response Header: Magic: %d Length: %d SC: %d\n", \
			magic_number, payload_length, status_code);
	pthread_mutex_unlock(&console_mutex);
	
	if(status_code != OK)
		return FAILURE;
	else
		return payload_length;			
}

/*
 * Composes a header and returns it as string.
 * Args:
 *  magic - Magic to be used in header
 *  len - Length to be used in header
 *  rc - RC to be used in header
 * Returns:
 *  Header as a string of bytes
 * */
static char* compose_header(uint32_t magic, uint16_t length, RC request_code){

    int offset = 0;
	char *header = calloc(1, HEADERLEN);
	if(!header) {
		perror("Malloc");
		exit(1);
	}
    uint16_t data_length = htons(length);
    uint16_t rc = htons((uint16_t)request_code);
    
    memcpy(header, &magic, sizeof(uint32_t));
    offset += sizeof(uint32_t);
    memcpy(header + offset, &data_length, sizeof(uint16_t));
    offset += sizeof(uint16_t);
    memcpy(header + offset, &rc, sizeof(uint16_t));
    return header;
}  

/*
 * Sends out single Large Compress request and prints the response.
 * Input is given as a filename. We read the file and use it as payload.
 * Args:
 *  sockfd - socket FD
 *  magic - Magic to be used in header
 *  len - Length to be used in header
 *  rc - RC to be used in header
 *  filename - File to be used
 *  valid - Just for testing(To inform that it should avoid reading payload)
 * Returns:
 *  None
 */
static void test_large_compress(int sockfd, uint32_t magic,\
								 RC rc, char *filename, int valid) {
    char *header;
    char payload[TMAXDATALEN];    
    char buffer[TMAXDATALEN];
    memset(payload, 0, TMAXDATALEN);
    memset(buffer, 0, TMAXDATALEN);
    int numbytes, fd;
				
	fd = open(filename, O_RDONLY);
	if(fd == FAILURE) {
		perror("Open");
		exit(FAILURE);
	}
	
	//Read file 
	int len = read_all(fd, payload, TMAXDATALEN) - 1;

	//COMPRESS TEST
	header = compose_header(magic, len, rc);	
	send(sockfd , header , HEADERLEN , 0);
	if(valid)
		send(sockfd, payload, len, 0);
		
	pthread_mutex_lock(&console_mutex);
	printf("Sent COMPRESS req (Magic: %d LEN: %d RC: %d)\n", magic, len, rc);	
	pthread_mutex_unlock(&console_mutex);
	/*if(valid)
		printf("Sent COMPRESS Payload [%s]\n", payload);*/
		
	numbytes = read(sockfd , buffer, HEADERLEN);
	int length = parse_header(buffer);
	
	if(length <= 0 || !valid)
		return;
		
	//read all
	numbytes = read_all(sockfd, buffer, length);
	buffer[numbytes] = '\0';
	
	pthread_mutex_lock(&console_mutex);	
	printf("Compress Read %d out of %d\n", numbytes, length);
	printf("Return payload: [%s]\n", buffer);
	pthread_mutex_unlock(&console_mutex);
	
	close(fd);
}

/*
 * Sends out single COMPRESS request and prints the response.
 * Args:
 *  sockfd - socket FD
 *  magic - Magic to be used in header
 *  len - Length to be used in header
 *  rc - RC to be used in header
 *  payload - payload to be used
 *  valid - Just for testing(To inform that it should avoid reading payload)
 * Returns:
 *  None
 */
static void test_compress(int sockfd, uint32_t magic, uint16_t len, RC rc,\
						 	char *payload, int valid) {
    char *header;
    char buffer[BUFFLEN];
    int numbytes;
	
	memset(buffer, 0, BUFFLEN);
	
	//COMPRESS TEST
	header = compose_header(magic, len, rc);	
	send(sockfd , header , HEADERLEN , 0);
	if(valid)
		send(sockfd, payload, len, 0);
		
	pthread_mutex_lock(&console_mutex);
	printf("Sent COMPRESS req (Magic: %d LEN: %d RC: %d)\n", magic, len, rc);	
	if(valid)
		printf("Sent COMPRESS Payload [%s]\n", payload);		
	pthread_mutex_unlock(&console_mutex);
	
	numbytes = read(sockfd , buffer, HEADERLEN);
	int length = parse_header(buffer);
	
	if(length <= 0 || !valid)
		return;
		
	//read all
	numbytes = read_all(sockfd, buffer, length);
	buffer[numbytes] = '\0';
	
	pthread_mutex_lock(&console_mutex);
	printf("Compress Read %d out of %d\n", numbytes, length);
	printf("Return payload: %s\n", buffer);
	pthread_mutex_unlock(&console_mutex);
}

/*
 * Sends out single PING request and prints the response.
 * Args:
 *  sockfd - socket FD
 *  magic - Magic to be used in header
 *  len - Length to be used in header
 *  rc - RC to be used in header
 * Returns:
 *  None
 */
static void test_ping(int sockfd, uint32_t magic, uint16_t len, RC rc) {

	char *header;
    int numbytes;
    char buffer[BUFFLEN];
    
	memset(buffer, 0, BUFFLEN);
	
	//PING TEST
	header = compose_header(magic, len, rc);		
	send(sockfd , header , HEADERLEN , 0 );	
	
	pthread_mutex_lock(&console_mutex);
	printf("Sent PING req (Magic: %d LEN: %d RC: %d)\n", magic, len, rc);
	pthread_mutex_unlock(&console_mutex);
	
	numbytes = read( sockfd , buffer, HEADERLEN);
	parse_header(buffer);
}

/*
 * Sends out single RST request and prints the response.
 * Args:
 *  sockfd - socket FD
 *  magic - Magic to be used in header
 *  len - Length to be used in header
 *  rc - RC to be used in header
 * Returns:
 *  None
 */
static void test_reset(int sockfd, uint32_t magic, uint16_t len, RC rc) {

	char *header;
    int numbytes;
    char buffer[BUFFLEN];
    
	memset(buffer, 0, BUFFLEN);
    	
	//RST TEST
	header = compose_header(magic, len, rc);		
	send(sockfd , header , HEADERLEN , 0 );	

	pthread_mutex_lock(&console_mutex);
	printf("Sent RST req (Magic: %d LEN: %d RC: %d)\n", magic, len, rc);
	pthread_mutex_unlock(&console_mutex);
	
	numbytes = read( sockfd , buffer, HEADERLEN);
	parse_header(buffer);
}

/*
 * Sends out single get request and prints the response.
 * Args:
 *  sockfd - socket FD
 *  magic - Magic to be used in header
 *  len - Length to be used in header
 *  rc - RC to be used in header
 * Returns:
 *  None
 */
static void test_get(int sockfd, uint32_t magic, uint16_t len, RC rc) {

	char *header;
    int numbytes;
    char buffer[BUFFLEN];
    
	memset(buffer, 0, BUFFLEN);
	
	//GET TEST
	header = compose_header(magic, len, rc);		
	send(sockfd , header , HEADERLEN , 0 );
		
	pthread_mutex_lock(&console_mutex);
	printf("Sent GET req (Magic: %d LEN: %d RC: %d)\n", magic, len, rc);
	pthread_mutex_unlock(&console_mutex);
	
	numbytes = read( sockfd , buffer, HEADERLEN);
	int result = parse_header(buffer);
	if(result == FAILURE)
		return;
	numbytes = read(sockfd, buffer, GET_PAYLOAD_LEN);
	print_stats(buffer);
}

/*
 * Test plan for Reset
 * Args:
 *  sockfd - socket FD
 * Returns:
 *  None
 */
static void testplan_reset(int sockfd) {

	pthread_mutex_lock(&console_mutex);
	printf("#######Testing RESET started############\n");
	pthread_mutex_unlock(&console_mutex);

	//valid RST
	test_reset(sockfd, MAGIC, 0, RST);
	//Invalid length
	test_reset(sockfd, MAGIC, -1, RST);
	//Invalid length
	test_reset(sockfd, MAGIC, 10000, RST);
	//Invalid RC
	test_reset(sockfd, MAGIC, 0, 10);
	//Invalid RC
	test_reset(sockfd, MAGIC, 0, 0);
	//Invalid MAGIC
	test_reset(sockfd, 100000, 0, 0);

	pthread_mutex_lock(&console_mutex);
	printf("#######Testing RESET Ended############\n");
	pthread_mutex_unlock(&console_mutex);
	
}

/*
 * Test plan for Get
 * Args:
 *  sockfd - socket FD
 * Returns:
 *  None
 */
static void testplan_get(int sockfd) {

	pthread_mutex_lock(&console_mutex);
	printf("#######Testing GET started############\n");
	pthread_mutex_unlock(&console_mutex);
	
	//Valid get Returns (64 + 200 + 0)	
	test_get(sockfd, MAGIC, 0, GET);
	//Invalid length
	test_get(sockfd, MAGIC, -1, GET);
	//Invalid length
	test_get(sockfd, MAGIC, 10000, GET);
	//Invalid RC
	test_get(sockfd, MAGIC, 0, 10);
	//Invalid RC
	test_get(sockfd, MAGIC, 0, 0);
	//Invalid MAGIC
	test_get(sockfd, 100000, 0, 0);
	
	pthread_mutex_lock(&console_mutex);
	printf("#######Testing GET Ended############\n");
	pthread_mutex_unlock(&console_mutex);
	
}

/*
 * Test plan for compress large payload.
 * Check files t*.txt
 * Args:
 *  sockfd - socket FD
 * Returns:
 *  None
 */
static void testplan_large_compress(int sockfd) {

	pthread_mutex_lock(&console_mutex);
	printf("#######Testing compress started############\n");
	pthread_mutex_unlock(&console_mutex);
	
	//Populate test files
	int idx;
	for(idx = 1; idx <= NUMFILES; idx++) {
		char filename[FNAMELEN];
		snprintf(filename, FNAMELEN, "t%d.txt", idx);
		
		pthread_mutex_lock(&console_mutex);
		printf("Testing File %s\n", filename);
		pthread_mutex_unlock(&console_mutex);

		test_large_compress(sockfd, MAGIC, COMPRESS, filename, 1);
	}
	pthread_mutex_lock(&console_mutex);
	printf("#######Testing large compress DONE############\n");
	pthread_mutex_unlock(&console_mutex);
}

/*
 * Test plan for compress
 * Args:
 *  sockfd - socket FD
 * Returns:
 *  None
 */
static void testplan_compress(int sockfd) {

	pthread_mutex_lock(&console_mutex);
	printf("#######Testing compress started############\n");
	pthread_mutex_unlock(&console_mutex);
	
	//valid by empty payload
  	test_compress(sockfd, MAGIC, 0, COMPRESS, "", 1);
	//valid payload Returns => 6a
  	test_compress(sockfd, MAGIC, 6, COMPRESS, "aaaaaa", 1);
	//valid payload Returns => abcdef
  	test_compress(sockfd, MAGIC, 6, COMPRESS, "abcdef", 1);
	//valid payload Returns => 10a
  	test_compress(sockfd, MAGIC, 10, COMPRESS, "aaaaaaaaaa", 1);
	//valid payload Returns => b4ab
  	test_compress(sockfd, MAGIC, 6, COMPRESS, "baaaab", 1);
	//valid payload Returns => 2b6a2c
  	test_compress(sockfd, MAGIC, 10, COMPRESS, "bbaaaaaacc", 1);
	//Invalid payload (Contains number in middle)
  	test_compress(sockfd, MAGIC, 10, COMPRESS, "bbaaaa1acc", 1);
	//Invalid payload (Contains number at start)
  	test_compress(sockfd, MAGIC, 10, COMPRESS, "2bbaaaaacc", 1);
	//Invalid payload (Contains number at end)
  	test_compress(sockfd, MAGIC, 10, COMPRESS, "bbaaaaacc0", 1);
	//Invalid payload (Contains UC at middle)
  	test_compress(sockfd, MAGIC, 10, COMPRESS, "bbaaaaXacc", 1);
	//Invalid payload (Contains UC at start)
  	test_compress(sockfd, MAGIC, 10, COMPRESS, "Vbbaaaaacc", 1);
	//Invalid payload (Contains UC at end)
  	test_compress(sockfd, MAGIC, 10, COMPRESS, "bbaaaaaccZ", 1);
	//Invalid payload (Contains Special char at middle)
  	test_compress(sockfd, MAGIC, 10, COMPRESS, "bbaa!aaacc", 1);
	//Invalid payload (Contains Special char at start)
  	test_compress(sockfd, MAGIC, 10, COMPRESS, "@bbaaaaacc", 1);
	//Invalid payload (Contains Special char at end)
  	test_compress(sockfd, MAGIC, 10, COMPRESS, "bbaaaaaac)", 1);
  	
	pthread_mutex_lock(&console_mutex);
	printf("#######Testing compress DONE############\n");
	pthread_mutex_unlock(&console_mutex);

}

/*
 * Test plan for ping
 * Args:
 *  sockfd - socket FD
 * Returns:
 *  None
 */
static void testplan_ping(int sockfd) {

	pthread_mutex_lock(&console_mutex);
	printf("#######Testing PING started############\n");
	pthread_mutex_unlock(&console_mutex);
	
	//Valid ping
	test_ping(sockfd, MAGIC, 0, PING);
	//Invalid length
	test_ping(sockfd, MAGIC, -1, PING);
	//Invalid length
	test_ping(sockfd, MAGIC, 10000, PING);
	//Invalid RC
	test_ping(sockfd, MAGIC, 0, 10);
	//invalid RC
	test_ping(sockfd, MAGIC, 0, 0);
	//Invalid magic
	test_ping(sockfd, 100000, 0, 0);
	
	pthread_mutex_lock(&console_mutex);
	printf("#######Testing PING DONE############\n");
	pthread_mutex_unlock(&console_mutex);

}

/*
 * See testplan.txt for explanation.
 * We are doing only the combination tests (GET + COMPRESS, GET + RST etc.)
 * Individual tests are done separately.
 * Args:
 *  sockfd - socket FD
 *  num - Test number (Reference in test_plan.txt
 * Returns:
 *  None
 */
static void testplan_combinations(int sockfd, int num) {

	switch(num) {
	
		case 0: test_reset(sockfd, MAGIC, 0, RST); //Ratio check
			testplan_compress(sockfd);
			test_get(sockfd, MAGIC, 0, GET);
			break;												
		case 4: test_reset(sockfd, MAGIC, 0, RST);
			test_get(sockfd, MAGIC, 0, GET);
			break;
		case 5: test_reset(sockfd, MAGIC, 0, RST);
			test_ping(sockfd, MAGIC, 0, PING);
			test_get(sockfd, MAGIC, 0, GET);
			break;
		case 19: test_reset(sockfd, MAGIC, 0, RST);
			test_compress(sockfd, MAGIC, 10, COMPRESS, "bbaaaaaac)", 1);
			test_get(sockfd, MAGIC, 0, GET);
			break;
		case 20: test_reset(sockfd, MAGIC, 0, RST);
			test_compress(sockfd, MAGIC, 6, COMPRESS, "aaaaab", 1);
			test_get(sockfd, MAGIC, 0, GET);
			break;
		case 21: test_reset(sockfd, MAGIC, 0, RST);
			test_compress(sockfd, MAGIC, 6, COMPRESS, "abcdef", 1);
			test_get(sockfd, MAGIC, 0, GET);
			break;
		case 24: test_reset(sockfd, MAGIC, 0, RST);
			test_large_compress(sockfd, MAGIC, COMPRESS, "t1.txt", 1);
			test_get(sockfd, MAGIC, 0, GET);
			break;
		case 25: test_reset(sockfd, MAGIC, 0, RST);
			test_large_compress(sockfd, MAGIC, COMPRESS, "t5.txt", 1);
			test_get(sockfd, MAGIC, 0, GET);
			break;		
		case 26: test_reset(sockfd, MAGIC, 0, RST);
			test_large_compress(sockfd, MAGIC, COMPRESS, "t4.txt", 1);
			test_get(sockfd, MAGIC, 0, GET);
			break;
		default:
			break;
	}
}

/*
 * Connects to server on HOST + PORT endpoint.
 */
static int connect_to_server() {

	int sockfd;  
	struct addrinfo hints, *servinfo, *p;
	int ret;
	
	memset(&hints, 0, sizeof hints);
	hints.ai_family = AF_UNSPEC;
	hints.ai_socktype = SOCK_STREAM;

	if ((ret = getaddrinfo(HOST, PORT, &hints, &servinfo)) != 0) {
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(ret));
		return -1;
	}

	// loop through all the results and connect to the first we can
	for(p = servinfo; p != NULL; p = p->ai_next) {
		if ((sockfd = socket(p->ai_family, p->ai_socktype,
								p->ai_protocol)) == -1) {
			perror("client: socket");
			continue;
		}

		if (connect(sockfd, p->ai_addr, p->ai_addrlen) == -1) {
			close(sockfd);
			perror("client: connect");
			continue;
		}

		break;
	}

	if (p == NULL) {
		fprintf(stderr, "client: failed to connect\n");
		return -1;
	}

	freeaddrinfo(servinfo); // all done with this structure
	return sockfd;
}

/*
 * When we want to test with single client, we can also set NCONCURRENCY = 1
 * comment/uncomment any testplan.
 **/
void single_client_test() {

	int sockfd = connect_to_server();
	if(sockfd == FAILURE)
		return;
 	
	//testplan_ping(sockfd);
	//testplan_reset(sockfd);
	//testplan_get(sockfd);    
	//testplan_compress(sockfd);
	
	//testplan_large_compress(sockfd);
	
	testplan_combinations(sockfd, 4);
	testplan_combinations(sockfd, 5);
	testplan_combinations(sockfd, 19);
	testplan_combinations(sockfd, 20);
	testplan_combinations(sockfd, 21);
	close(sockfd);
}

/*
 * Single thread executes test plan.
 * comment/uncomment any testplan.
 */
void *single_thread_test() {

	int sockfd = connect_to_server();
	if(sockfd == FAILURE)
		return NULL;
 	
	//testplan_ping(sockfd);
	//testplan_reset(sockfd);
	//testplan_get(sockfd);    
	//testplan_compress(sockfd);	
	
	//testplan_large_compress(sockfd);
	
	testplan_combinations(sockfd, 0);
	testplan_combinations(sockfd, 4);
	testplan_combinations(sockfd, 5);
	testplan_combinations(sockfd, 19);
	testplan_combinations(sockfd, 20);
	testplan_combinations(sockfd, 21);
	testplan_combinations(sockfd, 24);
	testplan_combinations(sockfd, 25);
	testplan_combinations(sockfd, 26);
	
	close(sockfd);
}

/*
 * Test with concurrent clients.
 * Create NCONCURRENCY threads and test parallely.
 * Test with NCONCURRENCY 1000 and testplan_large_compress passed
 *   - Mainly to test server load handling.
 */
void concurrent_clients_test() {

	pthread_t tid_arr[NCONNCURRENCY];
	int idx;
	
	for(idx = 0; idx < NCONNCURRENCY; idx++) {
		if(pthread_create(&tid_arr[idx], NULL, single_thread_test, NULL)) {
			perror("Thread create");
			return;
		}
	}
	for(idx = 0; idx < NCONNCURRENCY; idx++) {
		pthread_join(tid_arr[idx], NULL);
	}
	
}

int main(int argc, char const *argv[])
{
	//single_client_test();
	concurrent_clients_test();
	return SUCCESS;
}
