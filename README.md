# Compression Service

### 1. Target Platform

Following are the details of the target platform.

Category | Version
--- | --- 
OS | Ubuntu 16.04
GCC | 5.4.0
Thread Model | POSIX
Architecture | x86_64
Kernel | 4.13.0-26-generic

###### Some system-wide constants:
* Maximum number of events the Epoll interface now listens to is set to 200. We can change based on the system's capability.
* Maximum payload the system supports is 8128 Bytes. (8KB including header + payload)

### 2. Implementation

This server is implemented in C, using Linux `event poll interface`. 
We will take a look at the RequestEvent Structure which is the core of this event loop.
```
typedef struct RequestEvent {
	int fd;                     //Client's socket FD/ Server's listening FD
	int rst_flag;               //Flag to denote if stats are reset by this event.
	uint32_t event;             //Event could be either EPOLLIN/EPOLLOUT
	uint16_t offset, length;    //Offset from where to read/write data from the FD.
	                            //Length is the total length of payload.
	char *data;                 //Payload from the request.
	char header[64];            //Header from the request
} RequestEvent;

```
Whenever we register for an event on a `client socket's file descriptor`, 
we create an instance of this structure, update the fields and set `epoll_event.data.ptr` point to it's reference.
In this way, whenever we get notified about an event on this FD, we can retrieve this structure and proceed with read/write.
`RequestEvent->header` will store the header from the request(GET/RESET/PING/COMPRESS).
If there is any payload(COMPRESS), we allocate memory based on its length and store it in `RequestEvent->data`.

Following are the order of events that takes place when the service starts.

1. Server starts, creates a socket and binds to port 4000.
2. All socket FDs are set to non-blocking mode and registered for EPOLLIN/EPOLLOUT event using epoll_ctl interface.
3. Server starts the event loop with only the server's socket FD in the set of registered FDs.
4. Whenever some FDs become available for reading/writing, we first check the FD.
    * If its server FD, then we got a new connection. So we accept the connection. This new client socket FD is registered for EPOLLIN event.
    * If not, then we have to read/write on the client socket FD based on the event. We can retrieve the RequestEvent structure and proceed from there.
5. Read/Write may not finish in a single read()/write() system call. 
    * We have to check if the read/write is complete based on the number of bytes read/write so far. 
    * If the read/write is not complete, we again register the FD for EPOLLIN/EPOLLOUT event and wait for it to become ready.
6. When a read is finished, we process the data and write the data back to the client socket FD. 

We will see how each request is handled.

```
//status Codes
typedef enum SC{
	OK,                 //OK
	UNKNOWN,            //Unknown error
	MSGTOOLONG,         //Message too large
	UNSUPPORTEDRC,      //Unsupported Request Code
	LOWMEMORY = 33,		//Low memory (33)
	INVALIDMAGIC,		//Header doesn't contain proper magic number (34)
	INVALIDLENGTH,		//Payload length < 0 (35)
	INVALIDDATA,		//Compression data is invalid (36)
	INVALIDRATIO		//Ratio is invalid (>100) (37) (Applicable only to GET request)
}SC;
```

###### PING
Status Code could be either OK, LOWMEMORY or UNKNOWN.
We check the memory statistics using sysinfo() system call. 
* If the value is less than the threshold, we send LOWMEMORY.
* If the call to sysinfo() fails, we return UNKNOWN. (As the client doesn't have to know the exact reason, it's ok to send UNKNOWN)
* If everything is fine, we send OK.

###### GET
It populates the statistics and sends out accordingly. `ratio = (compressed bytes/Requested bytes) * 100`
Responses:

Status | Payload | Reason
--- | --- | --- 
OK | [Received Bytes + Sent Bytes + Ratio] | Valid request
MSGTOOLONG | Nil | length too large (>0)
INVALIDMAGIC | Nil | Invalid Magic field
INVALIDLENGTH | Nil | Length < 0
UNSUPPORTEDRC | Nil | Request code is not valid
INVALIDRATIO |  [Received Bytes + Sent Bytes + 100] | Ratio went > 100.(So, Compressed bytes and requested bytes are reset)
* Invalid Ratio error won't happen. But, in case the ratio goes > 100, we need a way of notifying clients.
* `The GET Request's header size(=64) and the (response header + payload = 200) size are included in GET response's Received Bytes and Sent Bytes values`
	* Ex. A fresh GET request to the system after restart will return [ 64 + 200 + 100] as payload. (+ is concatenation operator)
* `But the counter in our system is updated only after the request is written over the network successfully.`

###### RESET
* Resets the statistics. Status is set similar to GET request(Except INVALIDRATIO) based on the header values we get.
* We only reset the stats if the header is valid. 
* `When a reset happens, size of RESET response header is not added to Sent Bytes counter in the system.` So, after a reset, all the counters remain at 0.

###### COMPRESS
* In addition to the status codes returned by GET request(Except INVALIDRATIO), we send INVALIDDATA when the input contains character other than a-z.
* `Only when we successfully compress the payload, we update the Compressed bytes and Requested bytes counters.` If the data is invalid, we dont update the counters.

### 3. Assumptions
To highlight the assumptions made about the system,
* When a request header's MAGIC field is not proper or if the payload length field is too high, we close that particular connection after writing out the response. 
    * Invalid magic may occur if we go out of sync with the client. Reading further from wrong place only result in more failure responses.
    * Large payload length could be mistakenly written by client, (say 10000 instead of 100), we are more likely to go out of sync by reading the payload.
	* These are just assumptions, we can easily remove this check if we don't need it.
* When the counters overflow, we reset them to 0. 
    * If sent bytes/received bytes counter overflows, we reset both Sent bytes and Received bytes counters. 
    * If compressed bytes/requested bytes counter overflows, we reset both of them.
* Ratio is computed as ratio = (compressed bytes/Requested bytes) * 100. Where Compressed bytes is the number of bytes we have compressed out of requested bytes.
* When a reset happens, size of RESET response header is not added to Sent Bytes counter in the system.
    * If any request causes reset due to overflow, we don't add that particular response's value to the sent bytes counter. 
* The GET Request's header size(=64) and the (response header + payload = 200) size are included in GET response's Received Bytes and Sent Bytes values
    * A fresh GET request to the system after restart will return [ 64 + 200 + 100] as payload
    * But the counter in our system is updated only after the request is written over the network successfully.

### 4. Testing

* client.c offers single threaded test client and multithreaded test client.
* To test multithreaded client, we set NCONCURRENCY value accordingly.
See test_plan.txt for detailed test plan.

### 5. Future Improvements
This event driven architecture gives high performance and scalability over a thread-per-connection model. 
But this runs only on a single core of the machine, leaving other cores idle.
If we could create a set threads(one per core) and assign the request handling to each of them, it could be much faster.
But this needs synchronization and thread safety. Also, given the fact that all the requests except ping needs access to the shared resource(Byte counters), adding locks may create more thread contention.
Also, these request processing(Including COMPRESS) are not computationally heavy(All happens in-memory. Doesn't perform any I/O). So single core model is still powerful.

### 6. File hierarchy
1. src/ folder contains the source code for server
 * server.c contains the logic for server & event loop initialization. 
 * event_handler.c contains all the logic for this event loop handling.
 * request_handler.c contains logic for handling the requests.
2. test/ folder contains code for test client
3. include/ folder contains common.h (A shared header file between server and test client)

