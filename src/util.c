#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/epoll.h>

#include "common.h"

/*
 * Wrapper to epoll ctl interface.
 * Args:
 *  epfd - Epoll File Descriptor
 *  op - Operation (similar to epoll_ctl args)
 *  fd - File descriptor to be monitored (similar to epoll_ctl args)
 *  event - Epoll Event (similar to epoll_ctl args)
 *  data - Could be either a fd/RequestEvent struct
 * Returns:
 *  None
 * */
void epoll_ctl_wrapper(int epfd, int op, int fd, uint32_t event, void *data) {

	struct epoll_event n_event;
	
	n_event.events = event;
	n_event.data.ptr = data;
	
	if(epoll_ctl(epfd, op, fd, &n_event) == FAILURE) {
		perror("Epoll_Ctl");
		exit(FAILURE);
	}	
}
/*
 * Set the FD non-blocking
 * Args: 
 *  fd - File Descriptor to be made non-blocking (Mainly sockets)
 * Returns:
 *  None
 */
void set_nonblocking(int fd){

	int old_flags = fcntl(fd, F_GETFL, NULL);

	if(old_flags == FAILURE) {
		perror("Get FLAGS");
		exit(FAILURE);
	}

	int flags = old_flags | O_NONBLOCK;

	if(fcntl(fd, F_SETFL, flags) == FAILURE) {
		perror("Set FLAGS");
		exit(FAILURE);
	}
}

