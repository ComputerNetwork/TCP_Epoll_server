#ifndef _EVENT_HANDLER_H_
#define _EVENT_HANDLER_H_

/*
 * Author	: Sriramprabhu Sankaraguru
 * Email	: sriramprabhu.s@gmail.com
 * 
 * Event handler:
 * --------------
 * This file is responsible for
 *  - Accepting the incoming connections 
 *  - Handling Read & Write request whenever the FD is ready
 * */
 
void handle_incoming_connection(struct epoll_event event, int server_fd,\
								 int epoll_fd);
void handle_client_read(RequestEvent *request_event, int epoll_fd);
void handle_client_write(RequestEvent *request_event, int epoll_fd);
void handle_client_request(struct epoll_event event, int epoll_fd);								 

#endif
