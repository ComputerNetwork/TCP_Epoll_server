#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/epoll.h>
#include <fcntl.h>

#include "common.h"
#include "util.h"
#include "event_handler.h"
#include "request_handler.h"

/*
 * Cleanup resources/payload
 */
static void cleanup_resources(RequestEvent *request_event) {
	close(request_event->fd);
	if(request_event->data != NULL)
		free(request_event->data);
	request_event->data = NULL;
	free(request_event);
	request_event = NULL;
}

static void cleanup_payload(RequestEvent *request_event) {
	if(request_event->data) {
		free(request_event->data);
		request_event->data = NULL;
	}
}

/*
 * Handles Incoming client connections
 * Accept and add it to epoll_fd listening for EPOLLIN event 
 * Args:
 *  event - Epoll_Event structure
 *  server_fd - server socket FD
 *  epoll_fd - Epoll FD
 * Returns:
 *  None
 */
void handle_incoming_connection(struct epoll_event event, int server_fd,\
								 int epoll_fd) {
	
	struct sockaddr_in client_addr;
    socklen_t client_len = sizeof(client_addr);
	
	if(event.events & EPOLLHUP || event.events & EPOLLERR){
		printf("Error event on server fd: %d \n Exiting...\n", event.events);
		close(server_fd);
		exit(FAILURE);
	} 
	int client_fd = accept(server_fd, (struct sockaddr*)&client_addr,\
							 &client_len);
	if(client_fd == FAILURE){
		perror("Accept");
		exit(FAILURE);
	}
	if(DBG >= DBG_MSG)
		printf("Accepted New Connection from client\n");
	//set the fd non blocking
	set_nonblocking(client_fd);
	RequestEvent *request_event = calloc(1, sizeof(RequestEvent));
	request_event->fd = client_fd;
	//adding to epoll_fd listening for EPOLLIN event
	epoll_ctl_wrapper(epoll_fd, EPOLL_CTL_ADD, client_fd, EPOLLIN, \
						request_event);
}


/*
 * Read and parse the header.
 * Args:
 *  request_event - RequestEvent structure
 *  epoll_fd - Epoll FD
 * Returns:
 *  Success (0) if we need to read the payload.
 *  Failure (-1) otherwise
 */
static int handle_header_read(RequestEvent *request_event, int epoll_fd) {

	int fd = request_event->fd;
	int data_len = 0;
	
	//Read and parse Header
	if(read_client_header(request_event) == FAILURE) {
		//Connection is closed as the header couldn't be read
		return FAILURE;
	}
	
	update_received_bytes(request_event, HEADERLEN);	
	if(DBG >= DBG_ALL)
		print_stats();
		
	data_len = process_client_header(request_event);
	if(data_len == 0 ) {
		//No need to read payload. We can go ahead with write
		epoll_ctl_wrapper(epoll_fd, EPOLL_CTL_ADD, fd, EPOLLOUT, \
							request_event);
		return FAILURE;
	}		
	else if(data_len < 0) {
		//Header corruption. Close connection after writing the header.
		request_event->length = -1; //As its uint, it will be MAX value
		epoll_ctl_wrapper(epoll_fd, EPOLL_CTL_ADD, fd, EPOLLOUT, \
				request_event);		
		return FAILURE;	
	}
	else {
		if(DBG >= DBG_ALL)
			printf("Allocating Memory for payload: %d\n", data_len);
		request_event->data = calloc(1, data_len);
		if(!request_event->data) {
			perror("Malloc");
			exit(FAILURE);			
		}
		return SUCCESS;
	}

}

/*
 * Read the payload. 
 * If its partial, add EPOLLIN to read further 
 * If its complete, add EPOLLOUT for future write
 * Args:
 *  request_event - RequestEvent structure
 *  epoll_fd - Epoll FD
 * Returns:
 *  None 
 */
static void handle_payload_read(RequestEvent *request_event, int epoll_fd) {

	uint16_t data_len = request_event->length;
	uint16_t offset = request_event->offset;
	int fd = request_event->fd;
	
	//Read payload from client socket	
	int numbytes = read(fd, request_event->data + offset, data_len);

	if(numbytes == FAILURE) {
		perror("Client Read");
		cleanup_resources(request_event);
	}
	else if(numbytes == 0) {
		if(DBG >= DBG_MSG)
			printf("Client connection ended. closing client socket.\n");
		cleanup_resources(request_event);
	}
	else if(numbytes < data_len) {
		//partial read.
		request_event->offset += numbytes;	
		request_event->length -= numbytes;	
		update_received_bytes(request_event, numbytes);
		if(DBG >= DBG_ALL)
			printf("Got Payload from client Len: %d\n", numbytes);
		//Adding read event listener back
		epoll_ctl_wrapper(epoll_fd, EPOLL_CTL_ADD, fd, EPOLLIN, \
								request_event);
	}
	else {
		//Finished Reading
		update_received_bytes(request_event, numbytes);
		request_event->length = request_event->offset + numbytes;
		request_event->offset  = 0;
		if(DBG >= DBG_ALL) {
			printf("Got Payload from client Len: %d\n", numbytes);
			print_stats();
		}
		compress_payload(request_event);
		//Adding write event listener.
		epoll_ctl_wrapper(epoll_fd, EPOLL_CTL_ADD, fd, EPOLLOUT, \
							request_event);
	}
}

/*
 * Write header to the socket.
 * Args:
 *  request_event - RequestEvent structure
 *  epoll_fd - Epoll FD
 * Returns:
 *  Success (0) if we need to write the payload.
 *  Failure (-1) otherwise
 */
static int handle_header_write(RequestEvent *request_event, int epoll_fd) {

	int fd = request_event->fd;
	//Write header
	int numbytes = write(fd, request_event->header, HEADERLEN);
	//Failure
	if(numbytes == FAILURE) {
		if(errno == EINTR) {
			//Retry write later.
			if(DBG >= DBG_WARN)
				printf("EINTR on write header. retrying later\n");
			epoll_ctl_wrapper(epoll_fd, EPOLL_CTL_ADD, fd, EPOLLOUT, \
								request_event);
		}
		else {			
			perror("Write Header on client socket:");
			if(DBG >= DBG_WARN)
				printf("Closing the client connection\n");
			cleanup_resources(request_event);
		}
		return FAILURE;
	}
	else
		update_sent_bytes(request_event, HEADERLEN);
	if(DBG >= DBG_ALL) {
		printf("Successfully written header\n");
		print_stats();
	}
	return SUCCESS;
}

/*
 * Write the payload. 
 * If its partial, add EPOLLOUT to write further 
 * If its complete, add EPOLLIN for future read
 * Args:
 *  request_event - RequestEvent structure
 *  epoll_fd - Epoll FD
 * Returns:
 *  None 
*/
static void handle_payload_write(RequestEvent *request_event, int epoll_fd) {
	
	int fd = request_event->fd;
	uint16_t offset = request_event->offset;
	uint16_t length = request_event->length;
	
	//write payload
	int numbytes = write(fd, request_event->data + offset, length);	

	if(numbytes == FAILURE) {	
		//Failure
		if(errno == EINTR) {
			//Retry write later.
			epoll_ctl_wrapper(epoll_fd, EPOLL_CTL_ADD, fd, EPOLLOUT, \
								request_event);
		}
		else {			
			perror("Write payload on client socket");
			if(DBG >= DBG_WARN)
				printf("Closing the client connection\n");
			cleanup_resources(request_event);
		}
	}
	else if(numbytes < length) {	
		//Partial write
		//update length & offset
		request_event->length -= numbytes;
		request_event->offset += numbytes;
		update_sent_bytes(request_event, numbytes);
		//Retry write later
		epoll_ctl_wrapper(epoll_fd, EPOLL_CTL_ADD, fd, EPOLLOUT, \
								request_event);
	}
	else {	
		//completed write
		update_sent_bytes(request_event, numbytes);
		
		if(DBG >= DBG_MSG)
			printf("Write completed... adding read event\n");
		if(DBG >= DBG_ALL)
			print_stats();
		
		cleanup_payload(request_event);
		request_event->offset = 0;
		request_event->length = 0;
		request_event->rst_flag = 0;
		epoll_ctl_wrapper(epoll_fd, EPOLL_CTL_ADD, fd, EPOLLIN, \
								request_event);
	}
}
/*
 * handle_client_read()/handle_client_write():
 * handles Read/write operation on client socket.
 * Args:
 *  request_event - RequestEvent structure
 *  epoll_fd - Epoll FD
 * Returns:
 *  None
 * */
void handle_client_read(RequestEvent *request_event, int epoll_fd) {
		
	if(!request_event) {
		printf("Fatal: Request event is null\n");
		exit(FAILURE);
	}	
	//check if its a fresh read
	if(request_event->data == NULL){
		int fd = request_event->fd;
		//Handle header, Allocate memory and then read payload.
		if(handle_header_read(request_event, epoll_fd) == FAILURE) {
			return;
		}
		else {
			//We have to read payload. Add event listener
			epoll_ctl_wrapper(epoll_fd, EPOLL_CTL_ADD, fd, EPOLLIN, \
								request_event);
			return;
		}
	}
	//Read payload from client socket
	handle_payload_read(request_event, epoll_fd);
}

void handle_client_write(RequestEvent *request_event, int epoll_fd) {

	int fd = request_event->fd;
	uint16_t offset = request_event->offset;
	uint16_t length = request_event->length;

	//write header first (Check if header needs to be written)
	if(request_event->header[0] != '\0') {			
	
		//Successfully written the header.
		if(handle_header_write(request_event, epoll_fd) != FAILURE) {		
		
			request_event->header[0] = '\0';
					
			if(length > 0 && request_event->data != NULL) { 
				//Need to write payload. Add listener
				if(DBG >= DBG_ALL)
					printf("Adding listener for payload write\n");
				epoll_ctl_wrapper(epoll_fd, EPOLL_CTL_ADD, fd, EPOLLOUT, \
							request_event);		
				return;
			}
			else {
				//Done with this write. Next read starts fresh
				request_event->rst_flag = 0;
			}
			
		}
		else {
			//Either we have closed the connection or retrying write later.
			return;
		}
	}
	
	//Check if we need to write payload.
	if(length == 0) {
		cleanup_payload(request_event);
		request_event->offset = 0;
		request_event->rst_flag = 0;
		if(DBG >= DBG_MSG)
			printf("Write completed... adding read event\n");
		epoll_ctl_wrapper(epoll_fd, EPOLL_CTL_ADD, fd, EPOLLIN, \
								request_event);
		return;
	}
	//Input header is corrupted. So close the connection
	if(length == (uint16_t)FAILURE) {
		//clean up resources.
		if(DBG >= DBG_WARN)
			printf("Input Header was corrupted... Closing connection\n");
		cleanup_resources(request_event);	
		return;	
	}
	//Write payload to socket
	handle_payload_write(request_event, epoll_fd);
}
/*
 * Handle client requests
 * Args:
 *  event - Epoll_Event structure
 *  epoll_fd - Epoll FD
 * Returns:
 *  None
 */
void handle_client_request(struct epoll_event event, int epoll_fd) {

	//Get the request event structure
	RequestEvent *request_event = (RequestEvent *) event.data.ptr;		
	
	//Error event
	if(event.events & EPOLLHUP || event.events & EPOLLERR){
		if(DBG >= DBG_WARN)
			printf("Error event on client fd: %d \n Closing connection...\n",\
					 event.events);
		close(request_event->fd);
		free(request_event); 
		return;
	} 
	//FD Ready for read
	if(event.events == EPOLLIN){
		request_event->event = EPOLLIN;
		epoll_ctl_wrapper(epoll_fd, EPOLL_CTL_DEL, request_event->fd, 0, 0);
		handle_client_read(request_event, epoll_fd);
	}
	//FD Ready for write
	else if(event.events == EPOLLOUT){
		request_event->event = EPOLLOUT;	
		epoll_ctl_wrapper(epoll_fd, EPOLL_CTL_DEL, request_event->fd, 0, 0);
		handle_client_write(request_event, epoll_fd);
	}
}


