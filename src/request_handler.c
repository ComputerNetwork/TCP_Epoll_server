#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include <sys/sysinfo.h>

#include "common.h"
#include "util.h"
#include "request_handler.h"

//Statistics
static uint32_t received_bytes = 0;
static uint32_t sent_bytes = 0;
static uint32_t requested_bytes = 0;
static uint32_t compressed_bytes = 0;

/*
 * Compose header with given data
 * Args:
 *  Request_event - Structure that holds the header.
 *  length - Payload length
 *  s_code - Status Code Enum
 * Returns:
 *  None
 */
static void compose_header(RequestEvent *request_event, int length, SC s_code) {

	//header => 32bit magic + 16bit payload length + 16bit status code
	int offset = 0;
	uint16_t status_code;
	//Magic
	memcpy(request_event->header, &MAGIC, sizeof(MAGIC));
	offset += sizeof(uint32_t);
	//Length
	uint16_t len = htons(length);
	memcpy(request_event->header + offset, &(len), sizeof(uint16_t)); 
	offset += sizeof(uint16_t);
	//Status Code
	status_code = htons((uint16_t)s_code);
	memcpy(request_event->header + offset, &(status_code), sizeof(uint16_t)); 
	if(DBG >= DBG_MSG)
		printf("Header To client: Length: %d SC: %d\n", length, s_code);
	
}

/*
 * Payload is invalid. so let's clean up resources.
 * Args:
 *  Request_event - structure with all info.
 * Returns:
 * 	None
 * */
static void cleanup_payload(RequestEvent *request_event) {

	if(request_event->data){
		free(request_event->data);
		request_event->data = NULL;
	}	
	request_event->length = 0; 
	request_event->offset = 0;
}

/* Read sysinfo and return available memory
 * Returns:
 *  Available memory or -1 in case of failure.
 */
static int get_memory() {

	struct sysinfo info;
	
	if(sysinfo(&info) == FAILURE) {
		return FAILURE;
	}
	else {
		if(DBG >= DBG_ALL)
			printf("Sysinfo: Available memory: %ldB\n", info.freeram);
		return info.freeram;
	}
}

/*
 * Print statistics
 * Args:
 *  None
 * Returns:
 *  None
 */
void print_stats() {
	printf("RECVB: %d SENTB: %d CMPB: %d REQB: %d\n",\
			 received_bytes, sent_bytes, compressed_bytes, requested_bytes);
}

/*
 * Reset statistics to default values
 * Set the reset flag on request_event strucutre
 * Args:
 *  request_event - RequestEvent structure
 * Returns:
 *  None
 */
static void reset_stats(RequestEvent *request_event) {

	if(DBG >= DBG_MSG)
		printf("Resetting stats to default values\n");

	//Reset to default value. 
	compressed_bytes = 0;
	requested_bytes = 0;
	//Sent bytes should stay 0 even after writing this header
	received_bytes = 0;
	sent_bytes = 0;	
	request_event->rst_flag = 3;
}

/*
 * Processing Ping/Get/Reset/Compress header. 
 * In all cases, header is valid. Just change the logic based on request.
 * Args:
 *  request_event - Structure that holds all the info.
 * Returns:
 *  None
 */
static void process_ping_header(RequestEvent *request_event) {

	//return header	: 32bit magic + 16bit length(= 0) + 16bit status
	//status: OK/Low memory/Unknown error
	int memory = get_memory();
	
	if(DBG >= DBG_ALL)
		printf("Memory available: %d\n", memory);
	
	//Sysinfo failure. Client doesn't have to know the status. Send UNKNOWNERR
	if(memory == FAILURE)
		compose_header(request_event, 0, UNKNOWN);
	else {
		if(memory >= MEMORY_THRESHOLD)
			compose_header(request_event, 0, OK);
		else
			compose_header(request_event, 0, LOWMEMORY);
	}	
	return;
}

static void process_get_header(RequestEvent *request_event){

	int offset = 0;
	
	//Return header	: 32bit magic + 16bit length(32+32+8=72B) + 16bit status(OK)	
 	compose_header(request_event, GET_PAYLOAD_LEN, OK);
 	
	//Return Payload : <|requested_bytes | written_bytes | ratio|>
	request_event->data = calloc(1, GET_PAYLOAD_LEN);
	request_event->length = GET_PAYLOAD_LEN;
	
	//change it to NW byte order
	uint32_t r_bytes = htonl(received_bytes);
	//assuming successful write, Not updating original counters till we write.
	uint32_t s_bytes = htonl(sent_bytes + HEADERLEN + GET_PAYLOAD_LEN);	
	uint8_t performance_ratio = 0;
	
	if(requested_bytes == compressed_bytes || requested_bytes == 0) {
		performance_ratio = 100;
	}
	else if(compressed_bytes == 0) {
		performance_ratio = 0;	
	}	
	else {
		performance_ratio = (uint8_t)((double)compressed_bytes/requested_bytes \
												* 100);			
		if(performance_ratio > 100) {
			//strange... shouldn't happen.
			printf("CHECK: We have compressed more than requested %d\n",\
					 performance_ratio);
			performance_ratio = 100;
			//Reset stats.
			compressed_bytes = 0; 
			requested_bytes = 0;
			//Return Invalid Ratio error.
		 	compose_header(request_event, GET_PAYLOAD_LEN, INVALIDRATIO);
		}
	}
	
	memcpy(request_event->data, &r_bytes, sizeof(uint32_t));
	offset += sizeof(uint32_t);
	memcpy(request_event->data + offset, &s_bytes, sizeof(uint32_t));
	offset += sizeof(uint32_t);
	memcpy(request_event->data + offset, &performance_ratio, sizeof(uint8_t)); 
	
}

static void process_reset_header(RequestEvent *request_event) {

	//return header	: 32bit magic + 16bit length(= 0) + 16bit status
	//status: OK.
	compose_header(request_event, 0, OK);
	reset_stats(request_event);
}

static void process_compress_header(RequestEvent *request_event, int length) {

	//return header	: 32bit magic + 16bit length(= length) + 16bit status
	//status: OK for now. Might change later as We read and process the payload
	compose_header(request_event, length, OK);
	request_event->length = length;
}

/*
 * Update statistics 
 * Args: 
 *  request_event - Request Event structrue.
 *  count - Count to be added
 * Returns:
 *  None
 * Updates respective counter. If they overflow, the counters are reset to 0.
 * If the rst flag is set, this means the counters are reset by this request.
 * So we don't want to update the counters to keep consistency
 */
void update_received_bytes(RequestEvent *request_event, int count){

	if(request_event->rst_flag & 1 > 0) {
		//No need to update. As this job has reset this counters.
		return;
	}
	
	uint32_t old_rb = received_bytes;
	
	if(DBG >= DBG_ALL)
		printf("Adding %d to RecvB %d\n", count, received_bytes);
	
	received_bytes += count;
		
	if(received_bytes < old_rb) {
		printf("Resetting sent&recv bytes due to overflow");
		received_bytes = 0;
		sent_bytes = 0;
		request_event->rst_flag = 1;
	}	
}

void update_sent_bytes(RequestEvent *request_event, int count){

	if(request_event->rst_flag & 1 > 0) {
		//No need to update. As this job has reset this counters.
		return;
	}
	
	uint32_t old_sb = sent_bytes;
	
	if(DBG >= DBG_ALL)
		printf("Adding %d to SentB %d\n", count, sent_bytes);
	
	sent_bytes += count;
	
	if(sent_bytes < old_sb) {
		printf("Resetting sent&recv bytes due to overflow");
		received_bytes = 0;
		sent_bytes = 0;	
		request_event->rst_flag = 1;
	}
}

void update_requested_bytes(RequestEvent *request_event, int count) {

	if(request_event->rst_flag & 2 > 0) {
		//No need to update. As this job has reset this counters.
		return;
	}
	
	uint32_t old_rb = requested_bytes;
	
	if(DBG >= DBG_ALL)
		printf("Adding %d to ReqB %d\n", count, requested_bytes);
	
	requested_bytes += count;
	
	if(requested_bytes < old_rb) {
		printf("Resetting req & compressed bytes due to overflow");
		requested_bytes = 0;
		compressed_bytes = 0;
		request_event->rst_flag = 2;
	}
}

void update_compressed_bytes(RequestEvent *request_event, int count){

	if(request_event->rst_flag & 2 > 0) {
		//No need to update. As this job has reset this counters.
		return;
	}
	
	uint32_t old_cb = compressed_bytes;
	
	if(DBG >= DBG_ALL)
		printf("Adding %d to CompB %d\n", count, compressed_bytes);
	
	compressed_bytes += count;
	
	if(requested_bytes < old_cb) {
		printf("Resetting req & compressed bytes due to overflow");
		requested_bytes = 0;
		compressed_bytes = 0;
		request_event->rst_flag = 2;
	}
}

/*
 * All data for the request is received. Need to compress it and build header
 * Uses prefix encoding mechanism. Only works on valid characters (a-z)
 * We need to update the header & length as well.
 * Args:
 *  request_event - Structure that holds all the info
 * Returns:
 *  None
 */
void compress_payload(RequestEvent *request_event) {

	char *payload = request_event->data;
	if(!payload){
		compose_header(request_event, 0, INVALIDDATA);
		return;
	}	
	uint16_t length = request_event->length;
	int idx, compression_idx;
	int count = 1;
	char curr = payload[0];	
	
	for(idx = 1, compression_idx = 0; idx < length; idx++) {
		char next = payload[idx];
		if(next < 'a' || next > 'z' || curr < 'a' || curr > 'z') {
			if(DBG >= DBG_WARN)
				printf("Compress: Payload is invalid...%c->%c\n", curr, next);
			compose_header(request_event, 0, INVALIDDATA);
			cleanup_payload(request_event);
			return;
		}
		
		if(curr == next) 
			count++;	
					
		if(curr != next || idx == length - 1) {
			if(count > 1) {
				char count_str[5]; //Max count is 8192
				snprintf(count_str, 5, "%d", count);
				int i;
				for(i = 0; count_str[i] != '\0'; i++) {
					payload[compression_idx++] = count_str[i];
				}			
			}	
			payload[compression_idx++] = curr;	
			count = 1;
			curr = next;
		}			
	}
	
	//Check the last single char
	if( curr != payload[compression_idx-1] )
		payload[compression_idx++] = curr;	

	update_requested_bytes(request_event, length);
	length = compression_idx;
	update_compressed_bytes(request_event, length);
	
	if(DBG >= DBG_ALL)
		printf("Compressed string length: %d\n", length);
	
	request_event->length = length;
	request_event->offset = 0;
	
	//return header	: 32bit magic + 16bit length(= length) + 16bit status
	compose_header(request_event, length, OK);
}

/*
 * read client header
 * Args:
 *  request_event - RequestEvent Strucutre 
 * Returns:
 *  0 - If header is valid
 * -1 - If header data is corrupted/read failure
 */
int read_client_header(RequestEvent *request_event) {

	int fd = request_event->fd;
	//Read from client socket
	int numbytes = read(fd, request_event->header, HEADERLEN);
	
	if(numbytes == FAILURE) {
		perror("Client Read");
		close(fd);
		free(request_event->data);
		free(request_event);
		return FAILURE;
	}
	else if(numbytes == 0) {
		if(DBG >= DBG_MSG)
			printf("Client connection ended. closing client socket.\n");
		close(fd);
		free(request_event->data);
		free(request_event);
		return FAILURE;
	}
	else {
		if(DBG >= DBG_ALL)
			printf("Read Header from client Len: %d\n", numbytes);
	}
	return SUCCESS;
}

/*
 * Process client header
 * Args:
 *  request_event - RequestEvent Strucutre 
 * Returns:
 *  payload length
 *  Failure if the magic/length is invalid
 */
int process_client_header(RequestEvent *request_event) {

	int fd = request_event->fd;	
	uint32_t magic_number;
	uint16_t payload_length, request_code, status_code;
	SC s_code = OK;
	int offset = 0, ret = 0;
	
	memcpy(&magic_number, request_event->header + offset, sizeof(uint32_t));	
	offset += sizeof(uint32_t);
	memcpy(&payload_length, request_event->header + offset, sizeof(uint16_t));
	offset += sizeof(uint16_t);
	memcpy(&request_code, request_event->header + offset, sizeof(uint16_t));
	
	//Convert to host byte order
	payload_length = ntohs(payload_length);
	request_code = ntohs(request_code);	
	if(DBG >= DBG_ALL)
		printf("Parsed Client Header: Magic: %d Length: %d RC: %d\n", \
				magic_number, payload_length, request_code);
			
	//Invalid magic number
	if(magic_number != MAGIC) {
		if(DBG >= DBG_MSG)
			printf("Header Error: Magic field is invalid\n");
		s_code = INVALIDMAGIC;
		ret = FAILURE;
	}
	//Message is too large
	else if(payload_length > MAXDATALEN) {
		if(DBG >= DBG_MSG)
			printf("Header Error: Payload length is too high\n");
		s_code = MSGTOOLONG;	
		ret = FAILURE;
	}
	//invalid length field
	else if(payload_length < 0) {
		if(DBG >= DBG_MSG)
			printf("Header Error: Payload length is invalid\n");
		s_code = INVALIDLENGTH;
	}
	
	//We just have to write the header(Even in case of compression request.)
	if(s_code != OK){
		compose_header(request_event, 0, s_code);
		return ret;
	}
	
	//We need to check the request code and process accordingly.
	RC r_code = (RC)request_code;
	switch(r_code) {
		case PING: process_ping_header(request_event);
			break;
		case GET: process_get_header(request_event);
			break;
		case RST: process_reset_header(request_event);
			break;
		case COMPRESS: process_compress_header(request_event, payload_length);
			break;
		default: compose_header(request_event, 0, UNSUPPORTEDRC);
			return ret;
	}			
	//If its a compression request, we need to read data and write more
	return payload_length;
}

