#ifndef _UTIL_H_
#define _UTIL_H_
/*
 * Author	: Sriramprabhu Sankaraguru
 * Email	: sriramprabhu.s@gmail.com
 * 
 * Utils:
 * ----------------
 * This file is contains all the utility functions like,
 *  - Epoll ctl wrapper (Sets epoll_event.data.ptr accordingly)
 *  - Method to set a FD non-blocking
 * */
void epoll_ctl_wrapper(int epfd, int op, int fd, uint32_t event, void *data);
void set_nonblocking(int fd);

#endif
