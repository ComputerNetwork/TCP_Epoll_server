#ifndef _REQUEST_HANDLER_H_
#define _REQUEST_HANDLER_H_

/*
 * Author	: Sriramprabhu Sankaraguru
 * Email	: sriramprabhu.s@gmail.com
 * 
 * Request handler:
 * ----------------
 * This file is responsible for
 *  - Reading & Parsing header and payload from the RequestEvent structure
 *  - Handling the requests (GET/RST/PING/COMPRESSION) and build response
 *  - Maintains statistics.
 * */
 
int read_client_header(RequestEvent *request_event);
int process_client_header(RequestEvent *request_event);
void compress_payload(RequestEvent *request_event);

void update_received_bytes(RequestEvent *request_event, int count);
void update_sent_bytes(RequestEvent *request_event, int count);
void update_requested_bytes(RequestEvent *request_event, int count);
void update_compressed_bytes(RequestEvent *request_event, int count);

void print_stats();

#endif
