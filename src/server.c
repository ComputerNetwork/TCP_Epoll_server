#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <sys/epoll.h>

#include "common.h"
#include "util.h"
#include "event_handler.h"

/*
 * Author	: Sriramprabhu Sankaraguru
 * Email	: sriramprabhu.s@gmail.com
 * 
 * Compression Service:
 * --------------------
 * This service serves following requests from concurrent clients.
 * Ping			: Responds if the service is running properly
 * Get stats	: Returns the compression statistics
 * Reset stats	: Resets the compression Statistics.
 * Compress		: Compress the given string (Using prefix encoding)
 *
 * This file has the main function that starts the service.
 * As we don't expose any functionality outside, all methods stay static.
 *
 * References:
 * -----------
 * Beej's guide for network programming.
 * Few Blogs on Epoll.
 * */


/*
 * Creates and Bind socket 
 * Args:
 *	None
 * Returns:
 *  In case of failure returns -1
 *  Else returns Socket FD
 */
static int create_and_bind_socket(void)
{
	struct addrinfo hints;
	struct addrinfo *results, *r_ptr;
	int ret, socket_fd;

	memset(&hints, 0, sizeof(struct addrinfo));
	hints.ai_family = AF_UNSPEC; 
	hints.ai_socktype = SOCK_STREAM; //TCP socket
	hints.ai_flags = AI_PASSIVE;

	ret = getaddrinfo (NULL, PORT, &hints, &results);
	if(ret != SUCCESS)
	{
		fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(ret));
		return FAILURE;
	}
	//Go over the addresses and try to bind to one.
	for(r_ptr = results; r_ptr != NULL; r_ptr = r_ptr->ai_next)
	{
		socket_fd = socket(r_ptr->ai_family, r_ptr->ai_socktype, \
							r_ptr->ai_protocol);
		if(socket_fd == FAILURE){
			perror("Socket");
			continue;
		}
		//Bind the created socket
		ret = bind(socket_fd, r_ptr->ai_addr, r_ptr->ai_addrlen);
		if(ret == SUCCESS)
		{
			if(DBG >= DBG_MSG)
				printf("Successfully bound the socket\n");
			break;
		}

		close(socket_fd);
	}
	//Failure. couldn't bind
	if(r_ptr == NULL)
	{
		fprintf(stderr, "Bind Failure\n");
		return FAILURE;
	}
	//Free the address list
	freeaddrinfo(results);

	return socket_fd;
}

/*
 * starting event loop that handles all incoming connections.
 * Args:
 *  events - Array of Epoll event structure
 *  epoll_fd - Epoll FD
 *  server_fd - Server Socket FD
 * Returns:
 *  None
 * */
static 
void start_event_loop(struct epoll_event *events, int epoll_fd, int server_fd){

	//infinite loop
	while(1) {
		int idx;
		int event_count = epoll_wait(epoll_fd, events, MAXEVENTS, -1);
		if(event_count == FAILURE) {
			perror("Epoll Wait");
			exit(FAILURE);
		}
		for(idx = 0; idx < event_count; idx++) {
			if(events[idx].data.fd == server_fd) {
				//Handle server socket event(Incoming connection)
				handle_incoming_connection(events[idx], server_fd, epoll_fd);
			}				
			else{
				//Handle client request
				handle_client_request(events[idx], epoll_fd);
			}
		}
	}
	
}

int main(int argc, char **argv){

	int server_fd, epoll_fd, ret;
	struct epoll_event event, *events;
	server_fd = create_and_bind_socket();
	if(server_fd == FAILURE){
		printf("Couldn't create socket... exiting...\n");
		exit(FAILURE);
	}
	//Set the socket non-blocking
	set_nonblocking(server_fd);
	//start listening
	if(listen(server_fd, SOMAXCONN) == FAILURE) {
		perror("Listen");
		exit(FAILURE);
	}
	//Create Epoll FD
	if((epoll_fd = epoll_create(MAXEVENTS)) == FAILURE){
		perror("Epoll create");
		exit(FAILURE);
	};
	//Add socket to Epoll FD
	event.data.fd = server_fd;
	event.events = EPOLLIN;
	if(epoll_ctl(epoll_fd, EPOLL_CTL_ADD, server_fd, &event) == FAILURE) {
		perror("Epoll CTL");
		exit(FAILURE);
	}
	events = calloc(MAXEVENTS, sizeof(event));
	//Start event loop
	start_event_loop(events, epoll_fd, server_fd);
	//Done with event loop. Free up resources.
	free(events);
	close(epoll_fd);
	close(server_fd);
	return SUCCESS;
	
}

