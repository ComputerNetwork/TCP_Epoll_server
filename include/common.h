#ifndef _COMMON_H_
#define _COMMON_H_


#include <stdint.h>

//System-wide constants
static const char *PORT = "4000"; //Default Port
static const uint32_t MAGIC = 1498567763; //Magic number in Big-Endian
static const int FAILURE = -1;
static const int SUCCESS = 0;
static const int MAXEVENTS = 200; //Maximum Epoll events
static const int MAXDATALEN = 8128; //Maximum data in the compression request
static const int HEADERLEN = 64; //Header length.
static const int GET_PAYLOAD_LEN = 72; //Payload length for get request
static const int MEMORY_THRESHOLD = 20000; //Memory threshold(~20MB)

//Request Codes
typedef enum RC{
	PING = 1,
	GET,
	RST,
	COMPRESS
}RC;

//status Codes
typedef enum SC{
	OK,
	UNKNOWN,
	MSGTOOLONG,
	UNSUPPORTEDRC,
	LOWMEMORY = 33,		//Low memory
	INVALIDMAGIC,		//Header doesn't contain proper magic number
	INVALIDLENGTH,		//Payload length < 0
	INVALIDDATA,		//Compression data is invalid
	INVALIDRATIO		//Ratio is invalid (compressed > received)
}SC;

//Debug Level
typedef enum DBG_LEVEL{
	DBG_WARN,	//Only errors are printed
	DBG_MSG,	//Important messages are printed
	DBG_ALL		//Everything will be printed
} DBG_LEVEL;

static const DBG_LEVEL DBG = DBG_WARN;


/*
 * Request Event that holds:
 * Client's socket FD 
 * Flag to denote stats reset. (if its set, we don't add sent_bytes)
 * Event could be EPOLLIN/EPOLLOUT
 * length, offset are used in reading/writing chunks of data from/to socket
 * data is the payload, header is the actual header.
 */
typedef struct RequestEvent {
	int fd;
	int rst_flag;
	uint32_t event;
	uint16_t offset, length;
	char *data;
	char header[64];
} RequestEvent;

#endif
